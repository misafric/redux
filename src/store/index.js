import { configureStore } from '@reduxjs/toolkit';
//both createStore and configureStore create store

import counterReducer from './counter';
import authReducer from './auth';

const store = configureStore({
    reducer: { counter: counterReducer, auth: authReducer }
});

export default store;